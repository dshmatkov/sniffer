#include "tops.h"
#include "ui_tops.h"

Tops::Tops(QWidget *parent, QList<QString> volume_top, QList<QString> count_top) :
    QDialog(parent),
    ui(new Ui::Tops)
{
    ui->setupUi(this);
    for (int i = 0; i < volume_top.size(); ++i){
        this->ui->topVolume->insertItem(i, volume_top[i]);
        this->ui->topCount->insertItem(i, count_top[i]);
    }
}

Tops::~Tops()
{
    delete ui;
}
