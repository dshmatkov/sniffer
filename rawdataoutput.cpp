#include "rawdataoutput.h"
#include "ui_rawdataoutput.h"

RawDataOutput::RawDataOutput(QWidget *parent, RawData value) :
    QDialog(parent),
    ui(new Ui::RawDataOutput)
{
    this->data = value;
    ui->setupUi(this);
    QString temp;
    for (int i = 0; i < value.len; ++i){
        temp += QString::number((int)value.data[i], 16) + " ";
    }
    ui->rawData->setText(temp);
}

RawDataOutput::~RawDataOutput()
{
    delete ui;
}

void RawDataOutput::on_pushButton_clicked()
{
    Ethernet *ether = (Ethernet*)this->data.data;
    char *temp = (char*)malloc(this->data.len);

    QString result;
    for (int i = 0; i < ETHER_ADDR_LEN; ++i){
        if (i) result += ":";
        result += QString::number((int)ether->ether_shost[i], 16);
    }
    result += "\n";
    for (int i = 0; i < ETHER_ADDR_LEN; ++i){
        if (i) result += ":";
        result += QString::number((int)ether->ether_dhost[i], 16);
    }
    result += "\n";
    result += QString::number(ether->ether_type, 16);
    result += "\n";

    int current_position = sizeof(Ethernet);
    ip *ip_head = (ip*)(this->data.data + current_position);
    current_position += sizeof(ip);

    result += "src ip: " + QString(inet_ntoa(ip_head->ip_src)) + "\n";
    result += "dst ip: " + QString(inet_ntoa(ip_head->ip_dst)) + "\n";
    result += "header len: " + QString::number(ip_head->ip_hl) + "\n";
    result += "checksum: " + QString::number(ip_head->ip_sum) + "\n";

    switch (ip_head->ip_p){
        case IPPROTO_TCP:{
            tcphdr *tcp = (tcphdr*)(this->data.data + current_position);
            current_position += sizeof(tcphdr);
            result += "src port: " + QString::number(tcp->th_sport) + "\n";
            result += "dst port: " + QString::number(tcp->th_dport) + "\n";
            break;
        }
        case IPPROTO_UDP: {
            udphdr *udp = (udphdr*)(this->data.data + current_position);
            current_position += sizeof(udphdr);
            result += "src port: " + QString::number(udp->uh_sport) + "\n";
            result += "dst port: " + QString::number(udp->uh_dport) + "\n";
            break;
        }
        default:
            break;
    }
    for (int i = current_position; i < this->data.len; ++i){
        result += ((char)this->data.data[i]);
    }

    ui->rawData->setText(result);
}
