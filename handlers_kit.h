#ifndef HANDLERS_KIT
#define HANDLERS_KIT

#include <iostream>
#include <pcap/pcap.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <QObject>
#include <QString>
#include <QDateTime>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include "structsdefinition.h"

#define LOGS_DIR "logs/"
#define MAX_FRAME_LENGTH 65536
#define FORMAT_BASE 16

class HandlersKit : public QObject
{
    Q_OBJECT

public:
    static HandlersKit *getInstance();
    ~HandlersKit();
    RawData readCapturedDataFrame(int frameNumber);
    char* readStringCapturedDataFrame(int frameNumber);

public slots:
    void process();

signals:
    void PacketHandled(PacketInfo info);

private:
    HandlersKit();

    static HandlersKit *instance;
    QString logFileName;
    pcap_t *listening_device;
    u_char errorsMessage[1000];
    int frameNumber;
    int is_busy;

    static void handleDataFrame(u_char *buffer, const pcap_pkthdr *header, const u_char *packet);
    int writeCapturedDataFrame(RawData);
};

#endif // HANDLERS_KIT

