#ifndef TOPS_H
#define TOPS_H

#include <QDialog>

namespace Ui {
class Tops;
}

class Tops : public QDialog
{
    Q_OBJECT

public:
    explicit Tops(QWidget *parent = 0, QList<QString> volume_top = QList<QString>(), QList<QString> count_top = QList<QString>());
    ~Tops();

private:
    Ui::Tops *ui;
};

#endif // TOPS_H
