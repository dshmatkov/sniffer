#include "mainwindow.h"
#include <QApplication>

#pragma once

int main(int argc, char *argv[])
{
    qRegisterMetaType<PacketInfo>("PacketInfo");
    qRegisterMetaType<RawData>("RawData");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
