#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setCentralWidget(this->ui->mainWidget);

    this->ui->Packets->setColumnCount(COLUMNS_COUNT);
    this->ui->Packets->setRowCount(0);
    QList<QString> headers;
    headers.append("Source IP");
    headers.append("Destination IP");
    headers.append("Source Port");
    headers.append("Destination Port");
    headers.append("Size (bytes)");
    headers.append("Protocol");
    headers.append("Source Net Host");
    headers.append("Destination Net Host");

    this->ui->Packets->setHorizontalHeaderLabels(QStringList(headers));

    this->packetCountValues = new QMap<QString, int>();
    this->volumeValues = new QMap<QString, int>();

    this->kit = HandlersKit::getInstance();
    connect(
        this->kit, SIGNAL(PacketHandled(PacketInfo)),
        this, SLOT(handleParsed(PacketInfo))
    );

    this->catchingThread = new QThread();
    this->kit->moveToThread(this->catchingThread);

    connect(this->catchingThread, SIGNAL(started()), this->kit, SLOT(process()));
    this->catchingThread->start();

    connect(this->ui->TopBtn, SIGNAL(clicked(bool)), this, SLOT(getTops(bool)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleParsed(PacketInfo info){

    int col_count = this->ui->Packets->columnCount();
    int row = this->ui->Packets->rowCount();
    this->ui->Packets->setRowCount(row + 1);

    QString src_ip(info.src_ip_addr);

    if(!this->volumeValues->contains(src_ip)){
        this->volumeValues->insert(src_ip, 0);
    }
    this->volumeValues->values(src_ip)[0] += info.size;

    if (!this->packetCountValues->contains(src_ip)){
        this->packetCountValues->insert(src_ip, 0);
    }
    this->packetCountValues->values(src_ip)[0] += 1;

    QString colContent;

    for (int col = 0; col < col_count; ++col){
        colContent = "";
        switch(col){
            case 0: {
                colContent = QString(info.src_ip_addr);
                break;
            }
            case 1: {
                colContent = QString(info.dst_ip_addr);
                break;
            }
            case 2: {
                if (info.protocol != IPPROTO_TCP || info.protocol != IPPROTO_UDP) break;
                colContent = QString::number(info.source_port);
                break;
            }
            case 3: {
                if (info.protocol != IPPROTO_TCP || info.protocol != IPPROTO_UDP) break;
                colContent = QString::number(info.destination_port);
                break;
            }
            case 4: {
                colContent = QString::number(info.size);
                break;
            }
            case 5: {
                switch (info.protocol){
                    case IPPROTO_IP:
                        colContent = "IP";
                        break;
                    case IPPROTO_TCP:
                        colContent = "TCP";
                        break;
                    case IPPROTO_UDP:
                        colContent = "UDP";
                        break;
                    default:
                        colContent = "Unsupported";
                        break;
                }
                break;
            }
            case 6: {
                colContent = "";
                for (int i = 0; i < ETHER_ADDR_LEN; ++i){
                    if (i) colContent += ":";
                    colContent += QString::number((int)info.ether_shost[i], 16);
                }
                break;
            }
            case 7: {
                colContent = "";
                for (int i = 0; i < ETHER_ADDR_LEN; ++i){
                    if (i) colContent += ":";
                    colContent += QString::number((int)info.ether_dhost[i], 16);
                }
                break;
            }
            default:
                colContent = "";
                break;
        }
        QTableWidgetItem *item = new QTableWidgetItem(colContent);
        item->setFlags(item->flags() &= ~Qt::ItemIsEditable);
        this->ui->Packets->setItem(row, col, item);
    }
}

bool comparator(const QPair<QString, int> &a, const QPair<QString, int> &b){
    if (a.second > b.second) return false;
    return true;
}

void MainWindow::getTops(bool lala){
    QList<QString> volumeTop;
    QList<QString> countTop;

    QList<QString> items = this->volumeValues->keys();
    QVector< QPair<QString, int> > volume_pairs;
    QVector< QPair<QString, int> > count_pairs;
    for (int i = 0; i < items.size(); ++i){
        volume_pairs.push_back(
            QPair<QString, int>(
                items[i],
                this->volumeValues->values(items[i])[0]
            )
        );
        count_pairs.push_back(
            QPair<QString, int>(
                items[i],
                this->packetCountValues->values(items[i])[0]
            )
        );
    }

    qSort(volume_pairs.begin(), volume_pairs.end(), comparator);
    qSort(count_pairs.begin(), count_pairs.end(), comparator);

    for (int i = 0; i < std::min(volume_pairs.size(), 5); ++i){
        volumeTop.append(volume_pairs[i].first);
        countTop.append(count_pairs[i].first);
    }

    Tops *tops = new Tops(this, volumeTop, countTop);
    tops->show();
    tops->exec();
}


void MainWindow::on_Packets_doubleClicked(const QModelIndex &index)
{
    int row = index.row();

    RawDataOutput dataOutput(this, this->kit->readCapturedDataFrame(row));
    dataOutput.show();
    dataOutput.exec();
}
