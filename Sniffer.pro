#-------------------------------------------------
#
# Project created by QtCreator 2016-05-10T03:11:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Sniffer
TEMPLATE = app

LIBS += -lpcap


SOURCES += main.cpp\
        mainwindow.cpp \
        handlers_kit.cpp \
    rawdataoutput.cpp \
    tops.cpp

HEADERS  += mainwindow.h \
    handlers_kit.h \
    structsdefinition.h \
    rawdataoutput.h \
    tops.h

FORMS    += mainwindow.ui \
    rawdataoutput.ui \
    tops.ui
