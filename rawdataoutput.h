#ifndef RAWDATAOUTPUT_H
#define RAWDATAOUTPUT_H

#include <QDialog>
#include "structsdefinition.h"
#include "handlers_kit.h"

namespace Ui {
class RawDataOutput;
}

class RawDataOutput : public QDialog
{
    Q_OBJECT

public:
    explicit RawDataOutput(QWidget *parent = 0, RawData value=RawData());
    ~RawDataOutput();

private slots:
    void on_pushButton_clicked();

private:
    Ui::RawDataOutput *ui;
    RawData data;
};

#endif // RAWDATAOUTPUT_H
