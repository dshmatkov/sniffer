#ifndef STRUCTSDEFINITION
#define STRUCTSDEFINITION

#define SIZE_ETHERNET 14
#define ETHER_ADDR_LEN 6

#define IP_ADDR_LEN 16

struct Ethernet {
        u_char  ether_dhost[ETHER_ADDR_LEN];
        u_char  ether_shost[ETHER_ADDR_LEN];
        u_short ether_type;
};

struct PacketInfo {
    u_char ether_dhost[ETHER_ADDR_LEN];
    u_char ether_shost[ETHER_ADDR_LEN];
    char* src_ip_addr;
    char* dst_ip_addr;
    u_char protocol;
    long long source_port;
    long long destination_port;
    int size;
    int frameNumber;

    PacketInfo(){
        for (int i = 0; i < IP_ADDR_LEN; ++i){
            this->dst_ip_addr[i] = (char)0;
            this->src_ip_addr[i] = (char)0;
        }
        this->source_port = 0;
        this->destination_port = 0;
    }
};

struct RawData{
    int len;
    u_char *data;
};

#endif // STRUCTSDEFINITION

