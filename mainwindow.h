#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QModelIndex>
#include "handlers_kit.h"
#include "rawdataoutput.h"
#include "tops.h"

#define COLUMNS_COUNT 8

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void handleParsed(PacketInfo pinfo);
    void getTops(bool lala);

private slots:
    void on_Packets_doubleClicked(const QModelIndex &index);

private:
    Ui::MainWindow *ui;
    HandlersKit *kit;
    QThread *catchingThread;
    QMap<QString, int> *volumeValues;
    QMap<QString, int> *packetCountValues;
};

#endif // MAINWINDOW_H
