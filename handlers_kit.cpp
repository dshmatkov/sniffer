#include "handlers_kit.h"
#include <QFileInfo>

HandlersKit *HandlersKit::instance = 0;

void HandlersKit::handleDataFrame(u_char *buffer, const pcap_pkthdr *header, const u_char *packet){

    PacketInfo *collected_info = (PacketInfo*)malloc(sizeof(PacketInfo));

    const struct Ethernet *ethernet_layer = (Ethernet*)packet;
    const struct ip *ip_layer = (ip*)(packet + SIZE_ETHERNET);

    for (int i = 0; i < ETHER_ADDR_LEN; i++){
        collected_info->ether_dhost[i] = ethernet_layer->ether_dhost[i];
        collected_info->ether_shost[i] = ethernet_layer->ether_shost[i];
    }

    char *ip_addr = inet_ntoa(ip_layer->ip_dst);
    collected_info->src_ip_addr = (char*)malloc(strlen(ip_addr)+1);
    memcpy(collected_info->src_ip_addr, ip_addr, strlen(ip_addr));
    collected_info->src_ip_addr[strlen(ip_addr)] = (char)0;

    ip_addr = inet_ntoa(ip_layer->ip_src);
    collected_info->dst_ip_addr = (char*)malloc(strlen(ip_addr)+1);
    memcpy(collected_info->dst_ip_addr, ip_addr, strlen(ip_addr));
    collected_info->dst_ip_addr[strlen(ip_addr)] = (char)0;

    collected_info->protocol = ip_layer->ip_p;

    switch (ip_layer->ip_p){
        case IPPROTO_TCP:
            {
                const struct tcphdr *tcp_layer = (tcphdr*)(packet + SIZE_ETHERNET + sizeof(ip));
                collected_info->source_port = (int)tcp_layer->th_sport;
                collected_info->destination_port = (int)tcp_layer->th_dport;
            }
            break;
        case IPPROTO_UDP:
            {
                const struct udphdr *udp_layer = (udphdr*)(packet + SIZE_ETHERNET + sizeof(ip));
                collected_info->source_port = (long long)udp_layer->uh_sport;
                collected_info->destination_port = (long long)udp_layer->uh_dport;
            }
            break;
        default:
            break;
    }

    collected_info->size = header->len;

    RawData logData;
    logData.len = header->len;
    logData.data = (u_char*)malloc(header->len);
    memcpy(logData.data, packet, header->len);

    free(logData.data);

    int frameNumber = instance->writeCapturedDataFrame(logData);

    collected_info->frameNumber = frameNumber;

    emit instance->PacketHandled(*collected_info);
}

int HandlersKit::writeCapturedDataFrame(RawData frame){
    QFile logFile(this->logFileName);
    while (this->is_busy){
    }
    this->is_busy = true;
    while (!logFile.open(QIODevice::Append | QIODevice::Text)){
    }

    this->frameNumber++;
    QTextStream writeStream(&logFile);

    for (int i = 0; i < frame.len; ++i){
        writeStream << QString::number((uint)frame.data[i], FORMAT_BASE) << " ";
    }
    writeStream << "\n";

    int result = this->frameNumber;

    logFile.close();
    this->is_busy = false;
    return result;
}

RawData HandlersKit::readCapturedDataFrame(int frameNumber){
    QFile logFile(this->logFileName);
    while (!logFile.open(QIODevice::ReadOnly | QIODevice::Text)){
    }

    int maxLen = MAX_FRAME_LENGTH*4;
    char *rawFrame = (char*)malloc(maxLen * sizeof(char));

    for (int i = 0; i < frameNumber + 1; i++){
        logFile.readLine(rawFrame, (qint64)maxLen);
    }
    logFile.close();

    int len = 0;
    u_char *data = (u_char*)malloc(MAX_FRAME_LENGTH);
    QString number;

    for (int i = 0; rawFrame[i] != '\n';){
        if (rawFrame[i] == ' '){
            int value = number.toInt(NULL, FORMAT_BASE);
            data[len] = (u_char)value;
            len ++;
            number = "";
        }
        else{
            number += QChar::fromAscii(rawFrame[i]);
        }
        i++;

    }

    free(rawFrame);

    u_char *result_data = (u_char*)malloc(len);
    memcpy(result_data, data, len);
    free(data);

    RawData *result = (RawData*)malloc(sizeof(RawData));
    result->len = len;
    result->data = result_data;

    return *result;
}

char* HandlersKit::readStringCapturedDataFrame(int frameNumber){
    QFile logFile(this->logFileName);
    while (!logFile.open(QIODevice::ReadOnly | QIODevice::Text)){
    }

    int maxLen = MAX_FRAME_LENGTH*4;
    char *rawFrame = (char*)malloc(maxLen * sizeof(char));

    for (int i = 0; i < frameNumber; i++){
        logFile.readLine(rawFrame, (qint64)maxLen);
    }
    logFile.close();

    int len = 0;
    for (int i = 0; i < maxLen && rawFrame[i] != '\n'; ++i) len++;

    char *result = (char*)malloc(len);
    memcpy(result, rawFrame, len);

    free(rawFrame);

    return result;
}

void HandlersKit::process(){
    char error[100];
    this->listening_device = pcap_open_live("any", 65536, 1, 1000, error);
    pcap_loop(this->listening_device, 0, this->handleDataFrame, this->errorsMessage);
}

HandlersKit* HandlersKit::getInstance(){
    if (!instance){
        instance = new HandlersKit();
    }
    return instance;
}

HandlersKit::HandlersKit()
{
    QDir logsDir(LOGS_DIR);
    if (!logsDir.exists()){
        logsDir.mkdir(LOGS_DIR);
    }
    QDir::setCurrent(LOGS_DIR);

    QDateTime time;
    this->logFileName = time.currentDateTime().toString("yyyy-MM-dd--hh-mm-ss.log");
    this->frameNumber = 0;

    QFile logFile(LOGS_DIR + this->logFileName);
    logFile.open(QIODevice::WriteOnly | QIODevice::Text);
    logFile.close();
    this->is_busy = false;
}

HandlersKit::~HandlersKit()
{

}
